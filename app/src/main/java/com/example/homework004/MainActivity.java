package com.example.homework004;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FristFragment fristFragment = new FristFragment();
        SecondFragment secondFragment = new SecondFragment();
        FragmentManager fm = getSupportFragmentManager();

        if(findViewById(R.id.main_layout)!=null)
            fm.beginTransaction().add(R.id.main_layout,fristFragment).commit();

        if(findViewById(R.id.main_layout_land)!=null){
            fm.beginTransaction().replace(R.id.list_view_landscape,secondFragment)
                    .replace(R.id.text_view_landscape,fristFragment).commit();
        }

    }

}