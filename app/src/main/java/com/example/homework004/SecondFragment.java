package com.example.homework004;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class SecondFragment extends Fragment{

    RecyclerView recyclerView;
    List<MyEmailClass> EmailList;

    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_second, container, false);

        recyclerView = view.findViewById(R.id.recyclerview_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        CustomAdapter customAdapter = new CustomAdapter(IntiData(),getContext());

        recyclerView.setAdapter(customAdapter);
        return view;
    }

    private List<MyEmailClass> IntiData() {

        EmailList = new ArrayList<>();
        for(int i=1 ;i<20;i++){
            EmailList.add(new MyEmailClass("mok_sok@gmail"+i+".com"));
        }
        return EmailList;
    }

}