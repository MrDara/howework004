package com.example.homework004;

public class MyEmailClass {
    private String email;

    public MyEmailClass(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
