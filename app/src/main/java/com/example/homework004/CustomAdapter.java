package com.example.homework004;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder>{

    List<MyEmailClass> myEmail;
    Context context;
    public CustomAdapter(List<MyEmailClass> myEmail,Context context) {
        this.myEmail = myEmail;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view =layoutInflater.inflate(R.layout.list_email,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final MyEmailClass emailClass = myEmail.get(position);
        holder.text_email.setText(emailClass.getEmail());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Email : "+emailClass.getEmail(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return myEmail.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView text_email;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text_email = itemView.findViewById(R.id.txt_email);
        }
    }

}
