package com.example.homework004;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class FristFragment extends Fragment {

    Button btn_first;

    public FristFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       View view =  inflater.inflate(R.layout.fragment_frist, container, false);

       btn_first = view.findViewById(R.id.btn_first);

        if(Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation){
           btn_first.setVisibility(View.GONE);
       }

       btn_first.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               SecondFragment secondFragment = new SecondFragment();
               FragmentTransaction transaction =  getFragmentManager().beginTransaction();
               transaction.replace(R.id.main_layout,secondFragment);
               transaction.addToBackStack(null);
               transaction.commit();
           }
       });
       return view;
    }
}